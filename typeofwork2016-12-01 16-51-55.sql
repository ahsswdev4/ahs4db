Select id, typeofwork, location_id from ahs4db.typeofwork
USE ahs4db;

CREATE TABLE `typeofwork` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeofwork` varchar(20) DEFAULT NULL,
  `location_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


